package me.koenn.dbmanager.sql;

/**
 * SQL Field object.
 */
@SuppressWarnings("unused")
public class Field {

    /**
     * Name of the Field.
     */
    private final String name;

    /**
     * Field arguments.
     */
    private final String args;

    /**
     * Value of the Field.
     */
    private Object value;

    /**
     * Base Field constructor.
     *
     * @param name  Name of the Field.
     * @param value Value of the Field.
     * @param args  Field arguments.
     */
    public Field(String name, Object value, String args) {
        this.name = name;
        this.value = value;
        this.args = args;
    }

    public Field(String name, String args, int length) {
        this.name = name;
        this.args = args;
        this.value = "";
        for (int i = 0; i < length; i++) {
            this.value += "_";
        }
    }

    /**
     * Convert the Object type to a SQL data type.
     *
     * @param object Object to convert.
     * @return SQL data type
     */
    private static String getSqlType(Object object) {
        if (object instanceof Integer) {
            return "INT";
        } else if (object instanceof String) {
            if (((String) object).length() <= 255) {
                return "VARCHAR(" + ((String) object).length() + ")";
            } else {
                return "TEXT";
            }
        } else if (object instanceof Float) {
            return "FLOAT";
        } else if (object instanceof Double) {
            return "DOUBLE";
        } else {
            throw new IllegalArgumentException("Unable to convert " + object.getClass() + " to SQLType!");
        }
    }

    /**
     * Getters for the Field.
     */
    public String getName() {
        return this.name;
    }

    public Object getValue() {
        return this.value;
    }

    public String getArgs() {
        return this.args;
    }

    /**
     * Get the SQL format for the Field.
     *
     * @return SQL format
     */
    public String format() {
        return this.name + " " + getSqlType(this.value) + " " + this.args;
    }
}

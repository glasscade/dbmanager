package me.koenn.dbmanager.sql;

import me.koenn.dbmanager.helpers.Logger;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class QueryResult {

    private final List<List<String>> data = new ArrayList<>();

    public QueryResult(ResultSet resultSet) throws SQLException {
        ResultSetMetaData meta = resultSet.getMetaData();
        while (resultSet.next()) {
            List<String> rowData = new ArrayList<>();
            for (int i = 1; i < meta.getColumnCount() + 1; i++) {
                rowData.add(resultSet.getString(i));
            }
            data.add(rowData);
        }
    }

    public QueryResult(String resultSet) {
        Logger.debug(QueryResult.class, "Parsing: '" + resultSet + "'");
        String[] rows = resultSet.split("%");
        for (String row : rows) {
            List<String> rowData = new ArrayList<>();
            for (int i = 1; i < row.split("#").length; i++) {
                rowData.add(row.split("#")[i]);
            }
            data.add(rowData);
        }
    }

    public List<List<String>> getData() {
        return data;
    }

    @Override
    public String toString() {
        String result = "";
        for (List<String> row : data) {
            for (String value : row) {
                result += "#";
                result += value;
            }
            result += "%";
        }
        return result;
    }
}

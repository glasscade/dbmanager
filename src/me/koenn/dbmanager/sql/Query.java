package me.koenn.dbmanager.sql;

import me.koenn.dbmanager.helpers.Logger;
import me.koenn.networkingapi.tcp.TCPClient;

import java.io.File;
import java.io.IOException;
import java.sql.*;

/**
 * Easy Query Statement class.
 * Used to interact with a database File.
 */
public class Query {

    /**
     * Connection to the database.
     */
    private Connection connection;

    /**
     * Query statement.
     */
    private Statement statement;

    /**
     * SQL Query String.
     */
    private String sql;

    /**
     * Remote database client.
     */
    private TCPClient client;

    /**
     * Make a SQL Query to interact with a database.
     *
     * @param database Database File to interact with.
     * @param sql      SQL Query String to execute.
     * @throws SQLException
     */
    public Query(File database, String sql) throws SQLException {
        this.connection = DriverManager.getConnection("jdbc:sqlite:" + database.getAbsolutePath());
        this.statement = this.connection.createStatement();
        this.sql = sql;
        Logger.debug(this.getClass(), "Created local Query '" + this.toString().split("@")[1] + "' in db '" + database.getName() + "'.");
    }

    /**
     * Make a SQL Query to interact with a remote database.
     *
     * @param client Remote database connection.
     * @param sql    SQL Query String to execute.
     */
    public Query(TCPClient client, String sql) {
        this.sql = sql;
        this.client = client;
        Logger.debug(this.getClass(), "Created remote Query '" + this.toString().split("@")[1] + "' for remote database '" + client.toString().split("@")[1] + "'.");
    }

    /**
     * Execute the Statement with the SQL Query String.
     *
     * @throws SQLException
     */
    public QueryResult execute() throws SQLException {
        if (this.connection != null && !this.connection.isClosed()) {
            if (this.sql.startsWith("SELECT")) {
                Logger.debug(Query.class, "Getting ResultSet: '" + this.sql + "'");
                ResultSet resultSet = this.statement.executeQuery(this.sql);
                QueryResult result = new QueryResult(resultSet);
                this.statement.close();
                this.connection.close();
                return result;
            } else {
                this.statement.executeUpdate(this.sql);
                this.statement.close();
                this.connection.close();
            }
        } else {
            try {
                Logger.debug(Query.class, "Sending to server '" + this.sql + "'");
                this.client.getOutToServer().write(this.sql + '\n');
                this.client.getOutToServer().flush();
                Logger.debug(Query.class, "Waiting for response from server...");
                if (this.sql.startsWith("SELECT")) {
                    String line = this.client.getInFromServer().readLine();
                    if (line == null) {
                        line = this.client.getInFromServer().readLine();
                    }
                    return new QueryResult(line);
                }
                this.client.getOutToServer().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Logger.debug(this.getClass(), "Executed Query '" + this.toString().split("@")[1] + "'.");
        return null;
    }

    public void cancel() {
        try {
            if (this.statement != null && !this.statement.isClosed()) {
                this.statement.close();
            }
            if (this.connection != null && !this.connection.isClosed()) {
                this.connection.close();
            }
            Logger.debug(this.getClass(), "Cancelled Query '" + this.toString().split("@")[1] + "'.");
        } catch (SQLException ex) {
            Logger.error(Query.class, "Error while cancelling Query: " + ex.toString() + " " + ex.getMessage());
        }
    }
}

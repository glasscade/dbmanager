package me.koenn.dbmanager.helpers;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Simple automatic logging helper.
 */
public class Logger {

    /**
     * Log message format.
     */
    private static final String FORMAT = "[$DATETIME] [$TYPE] [$CLASS]: $MESSAGE";

    /**
     * Debug mode field.
     */
    private static final boolean DEBUG = true;

    /**
     * Format the log message with the right parameters.
     *
     * @param type    Logger Type
     * @param cl      Sender class
     * @param message Log message
     * @return Formatted log message
     */
    private static String getFormat(Type type, Class cl, String message) {
        String timestamp = new SimpleDateFormat("HH:mm:ss").format(new Date());
        return FORMAT
                .replace("$DATETIME", timestamp)
                .replace("$TYPE", type.toString())
                .replace("$CLASS", cl.getSimpleName())
                .replace("$MESSAGE", message);
    }

    /**
     * Send an info log message.
     *
     * @param cl  Sender class
     * @param msg Log message
     */
    public static void info(Class cl, String msg) {
        System.out.println(getFormat(Type.INFO, cl, msg));
    }

    /**
     * Send a debug log message.
     *
     * @param cl  Sender class
     * @param msg Log message
     */
    public static void debug(Class cl, String msg) {
        if (DEBUG) {
            System.out.println(getFormat(Type.DEBUG, cl, msg));
        }
    }

    /**
     * Send a warning log message.
     *
     * @param cl  Sender class
     * @param msg Log message
     */
    public static void warn(Class cl, String msg) {
        System.out.println(getFormat(Type.WARNING, cl, msg));
    }

    /**
     * Send an error log message.
     *
     * @param cl  Sender class
     * @param msg Log message
     */
    public static void error(Class cl, String msg) {
        System.err.println(getFormat(Type.ERROR, cl, msg));
    }

    /**
     * Logger types.
     */
    private enum Type {
        INFO, DEBUG, WARNING, ERROR
    }
}

package me.koenn.dbmanager.helpers;

import me.koenn.dbmanager.sql.Field;
import me.koenn.dbmanager.sql.Query;
import me.koenn.networkingapi.tcp.TCPClient;

import java.sql.SQLException;

/**
 * Helper methods for executing certain Queries.
 */
@SuppressWarnings("unused")
public class QueryHelper {

    /**
     * Execute the CREATE TABLE Query.
     *
     * @param name   Name of the Table
     * @param fields Fields of the Table
     */
    public static void createTable(String name, TCPClient database, Field... fields) {
        Query query = null;
        try {
            String sql = "CREATE TABLE " + name + " (";
            for (int i = 0; i < fields.length; i++) {
                if (i != fields.length - 1) {
                    sql += fields[i].format() + ", ";
                } else {
                    sql += fields[i].format() + ");";
                }
            }
            Logger.debug(QueryHelper.class, "Creating table '" + name + "'...");
            query = new Query(database, sql);
            query.execute();
            Logger.debug(QueryHelper.class, "Table '" + name + "' created.");
        } catch (SQLException ex) {
            if (ex.getMessage().contains("table " + name + " already exists")) {
                Logger.debug(QueryHelper.class, "Table '" + name + "' already exists so it can not be created.");
                if (query != null) {
                    query.cancel();
                }
            } else {
                Logger.error(QueryHelper.class, "Error in SQL: " + ex.getMessage());
            }
        }
    }
}

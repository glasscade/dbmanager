package me.koenn.dbmanager.server;

import me.koenn.dbmanager.DBManager;
import me.koenn.dbmanager.helpers.Logger;
import me.koenn.dbmanager.sql.Query;
import me.koenn.dbmanager.sql.QueryResult;
import me.koenn.networkingapi.tcp.TCPServer;

import java.io.IOException;
import java.net.Socket;
import java.sql.SQLException;

public class DataServer extends TCPServer {

    public DataServer() {
        super(8686);
    }

    @Override
    protected void onConnect(Socket socket) {
        try {
            String sql = this.getInFromClient().readLine();
            Query query = new Query(DBManager.DATABASE, sql);
            QueryResult queryResult = query.execute();
            if (queryResult != null) {
                Logger.debug(DataServer.class, "Sending '" + queryResult.toString() + "' to client...");
                this.getOutToClient().write(queryResult.toString());
                this.getOutToClient().flush();
                this.getOutToClient().close();
                Logger.debug(DataServer.class, "Done!");
            }
        } catch (IOException e) {
            Logger.error(DataServer.class, "Error while reading client message: " + e.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

package me.koenn.dbmanager;

import me.koenn.dbmanager.helpers.Logger;
import me.koenn.dbmanager.server.DataServer;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * Main class for the DBManager.
 */
public class DBManager {

    /**
     * Current DBManager version.
     */
    public static final float VERSION = 1.0F;

    /**
     * Main DBManager database.
     */
    public static final File DATABASE = new File("GlassCade.db");

    /**
     * Current DBManager instance.
     */
    public static DBManager instance;

    /**
     * DBManager status.
     */
    private boolean enabled = false;

    /**
     * DBManager static main method.
     *
     * @param args Program arguments
     */
    public static void main(String[] args) {
        instance = new DBManager();
        instance.start(args);
    }

    /**
     * DBManager startup method.
     *
     * @param args Program arguments
     */
    public void start(String args[]) {
        this.enabled = true;
        Logger.info(DBManager.class, "Starting DBManager v" + VERSION + "!");
        try {
            boolean server;
            try {
                server = Boolean.parseBoolean(args[0]);
            } catch (Exception ex) {
                server = false;
            }

            if (!DATABASE.exists()) {
                DATABASE.createNewFile();
            }

            this.loadJDBC();
            this.loadNetworkingAPI();

            DataServer dataServer = null;
            if (server) {
                dataServer = new DataServer();
                dataServer.start();
            }

            BufferedReader consoleInput = new BufferedReader(new InputStreamReader(System.in));
            while (enabled) {
                //System.out.print('>');
                String input = consoleInput.readLine();
                switch (input) {
                    case "quit":
                        if (server && dataServer != null) {
                            dataServer.stop();
                        }
                        this.quit();
                        System.exit(0);
                        break;
                    default:
                        Logger.warn(DBManager.class, "Unknown command '" + input + "'.");
                        break;
                }
            }
        } catch (Exception ex) {
            Logger.error(DBManager.class, "An uncaught error was found: " + ex.toString());
            ex.printStackTrace();
        }
    }

    /**
     * Load the JDBC package.
     */
    private void loadJDBC() {
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            Logger.error(DBManager.class, "Unable to load 'sqlite-jdbc-<version>.jar' from current directory!");
            System.exit(1);
        }
    }

    /**
     * Load the KoennNetworkingAPI.
     */
    private void loadNetworkingAPI() {
        try {
            File file = new File("KoennNetworkingAPI.jar");
            URL url = file.toURI().toURL();

            URLClassLoader classLoader = (URLClassLoader) ClassLoader.getSystemClassLoader();
            Method method = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
            method.setAccessible(true);
            method.invoke(classLoader, url);
        } catch (MalformedURLException | NoSuchMethodException | InvocationTargetException | IllegalAccessException ex) {
            Logger.error(DBManager.class, "Unable to load 'KoennNetworkingAPI.jar' from current directory!");
            System.exit(1);
        }
    }

    /**
     * Close the DBManager and quit the application.
     */
    public void quit() {
        this.enabled = false;
        Logger.info(DBManager.class, "Stopping DBManager v" + VERSION + "!");
    }
}
